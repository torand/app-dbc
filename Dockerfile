FROM perl:latest AS dev
MAINTAINER Tore Andersson

RUN apt update \
 && apt install -y \
    apt-utils \
    nano \
    less

ENV PERL_CPANM_HOME=/tmp/cpanmworkdir

WORKDIR /opt/dbc
COPY cpanfile .
RUN cpanm --self-contained --installdeps --notest .
RUN rm -rf $PERL_CPANM_HOME
RUN rm cpanfile

STOPSIGNAL TERM
CMD ["perl", "-E", "my $r=1; $SIG{INT}=$SIG{TERM}=sub {$r=0}; while($r) {sleep 600}; exit(0);"]

FROM dev AS benchmark

RUN apt update && apt install -y libaio-dev wget iproute2

COPY oracle-install-files /opt/oracle
WORKDIR /opt/oracle

ENV ORACLE_HOME=/opt/oracle/
ENV PATH=$PATH:$ORACLE_HOME
ENV LD_LIBRARY_PATH=$ORACLE_HOME
ENV TNS_ADMIN=/opt/dbc/t/oracle/wallet

RUN wget -t 3 -T 10 -w 3 -i files-to-download.txt

RUN ls *.zip |xargs -I {} bash -c "unzip -q -t {} && unzip -quo {}"
RUN rm *.zip
RUN bash -c "mv instantclient*/* ."

WORKDIR /opt/dbc

RUN cpanm DBD::Oracle DBD::MariaDB \
 && rm -rf $PERL_CPANM_HOME
