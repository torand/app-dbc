#!/usr/bin/env bash
set -e

docker-compose up testcoverage
firefox cover_db/coverage.html
