use 5.012;
use strict;
use warnings;

use Carp;
use Data::Dumper;
use Mojo::File qw(tempfile);
use Test::Most;

require './bin/dbc';

local $SIG{__DIE__} = sub { confess @_ };

my $tempfile = Mojo::File->new(tempfile);
ok $tempfile->remove, 'Remove tempfile so the script can create it';
ok( Mojo::File->new('t/testdata/dbc.conf')->copy_to($tempfile),
  'copied in ' );
local $ENV{APP_DBC_CONFIGFILE} = $tempfile;

END {
  $tempfile->remove;
}

{
  my ( $exitcode, $stdout ) = _run_params( [] );
  my $expected =
        "$tempfile - auto\n"
      . "$tempfile - log\n"
      . "$tempfile - logger\n"
      . "$tempfile - mariadb\n"
      . "$tempfile - mem\n"
      . "$tempfile - on_connect_magic\n"
      . "$tempfile - oracle\n"
      . "$tempfile - sqlite\n";
  is $stdout, $expected, 'no params';
}

{
  my ( $exitcode, $stdout ) = _run_params( ['--verbose=2'] );

  like $stdout, qr{$tempfile - auto\s+no description}, 'verbose=2 auto';
  like $stdout,
      qr{$tempfile - mem\s+:memory: based sqlite database},
      'verbose=2 mem';
  like $stdout,
      qr{$tempfile - on_connect_magic\s+showcase the on_connect_execute feature},
      'verbose=2 on_connect_magic';
  like $stdout,
      qr{$tempfile - sqlite\s+/dev/shm file based sqlite database},
      'verbose=2 sqlite';
}

done_testing;

sub _run_params {
  my ($app_param) = @_;

  my $stdout_var = '';
  open( my $stdout, '>', \$stdout_var ) or die $!;
  local *STDOUT = $stdout;
  my $rc = App::dbc::main( @{$app_param} );

  return $rc, $stdout_var;
}

__END__
