CREATE OR REPLACE DATABASE benchmark;
USE benchmark;

CREATE OR REPLACE TABLE `key_value_lookup` (
  `key` varchar(50) NOT NULL,
  `val` varchar(200),
  PRIMARY KEY (`key`)
);

CREATE OR REPLACE TABLE `key_value_updated` (
  `key` varchar(50) NOT NULL,
  `val` varchar(200),
  `ts`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
);

CREATE OR REPLACE TABLE `auto_incrementing_table` (
  `id`  int(11)      NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `ts`  timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `val` varchar(32)
);

