use 5.012;
use strict;
use warnings;

use Carp;
use Data::Dumper;
use Mojo::File qw(tempfile);
use Test::Most;

require './bin/dbc';

local $SIG{__DIE__} = sub { confess @_ };

my $tempfile = Mojo::File->new(tempfile);
ok $tempfile->remove, 'Remove tempfile so the script can create it';

{
  my ( $exitcode, $stdout ) = _run_params( $tempfile->to_string, "n\n",
    [ 'newdb', 'select 1 as true' ] );

  like $stdout, qr/A database connection for alias: newdb, must be define/,
      'ask to defined db connection';
  ok $tempfile->remove, 'Remove tempfile we are done with it';
}

{
  my ( $exitcode, $stdout ) = _run_params( $tempfile->to_string, "y\n9\n",
    [ 'newdb', 'select 1 as true' ] );
  like $stdout, qr/invalid option/,
      'invalid option error unless valid fiel selected';
  ok $tempfile->remove, 'Remove tempfile we are done with it';
}

{
  my ( $exitcode, $stdout ) =
      _run_params( $tempfile->to_string, "y\n3\n\n\n\n",
    [ 'newdb', 'select 1 as true' ] );
  like $stdout, qr/A database connection for alias: newdb, must be defined/,
      'ask to defined db connection';
  ok $tempfile->remove, 'Remove tempfile we are done with it';
}

{
  my ( $exitcode, $stdout ) = _run_params(
    $tempfile->to_string,
    "y\n3\nDBI:SQLite::memory:\n\n\n",
    [ 'newdb', 'select 1 as true', '--outformat=vertical' ]
  );
  ok -e $tempfile, 'config file created OK';
  like $stdout, qr/true: 1/, 'print expected query result';
}

{    # Test functions with minimal config provided as default tests are with
     # all options used in config file

  {  # YAML
    my ( $exitcode, $stdout ) = _run_params( $tempfile->to_string, "n\n",
      [ 'newdb', 'select 1 as true', '--outformat=yaml' ] );
    is $stdout, "---\ntrue: 1\n";
  }

  {    # JSON
    my ( $exitcode, $stdout ) = _run_params( $tempfile->to_string, "n\n",
      [ 'newdb', 'select 1 as true', '--outformat=json' ] );
    is $stdout, qq[{"true":1}\n];
  }

  {    # CSV
    my ( $exitcode, $stdout ) = _run_params( $tempfile->to_string, "n\n",
      [ 'newdb', 'select 1 as true', '--outformat=csv' ] );
    is $stdout, "true\n1\n";
  }

  {    # table
    my ( $exitcode, $stdout ) = _run_params( $tempfile->to_string, "n\n",
      [ 'newdb', 'select null as false', '--outformat=table' ] );
    is $stdout, "+-------+\n| false |\n+-------+\n|       |\n+-------+\n";
  }

  {    # STDIN
    my ( $exitcode, $stdout ) = _run_params(
      $tempfile->to_string,
      "SELECT 'a' AS COL;\n",
      [ 'newdb', '--sqlfile=STDIN', '--outformat=json' ]
    );
    is $stdout, qq[{"COL":"a"}\n], 'SQL from STDIN OK single value';
  }

  {    # STDIN - csv
    my ( $exitcode, $stdout ) = _run_params(
      $tempfile->to_string,
      qq{1,one\n2,two\n},
      [ 'newdb',           'SELECT ? A, ? B',
        '--optfile=STDIN', '--outformat=csv',
        '--informat=csv',  '--loop'
      ]
    );
    is $stdout, qq[A,B\n1,one\n2,two\n], 'SQL from STDIN OK loop';
  }

  {    # STDIN - csv
    my ( $exitcode, $stdout ) = _run_params(
      $tempfile->to_string,
      qq{1,one\n2,two\n},
      [ 'newdb',           'SELECT ? A, ? B, ? C, ? D',
        '--optfile=STDIN', '--outformat=csv',
        '--informat=csv'
      ]
    );
    is $stdout, qq[A,B,C,D\n1,one,2,two\n], 'SQL from STDIN OK not loop';
  }
}

done_testing;

sub _run_params {
  my ( $tempfile, $stdin_data, $app_param ) = @_;

  local $ENV{APP_DBC_CONFIGFILE} = $tempfile;

  my $stdin_var = $stdin_data;
  open( my $stdin, '<', \$stdin_var ) or die $!;
  local *STDIN = $stdin;

  my $stdout_var = '';
  open( my $stdout, '>', \$stdout_var ) or die $!;
  local *STDOUT = $stdout;
  my $rc = App::dbc::main( @{$app_param} );

  return $rc, $stdout_var;
}

__END__
