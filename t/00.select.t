use 5.012;
use strict;
use warnings;

use Carp;
use Data::Dumper;
use List::MoreUtils qw(uniq);
use Mojo::File      qw(tempfile);
use Test::Most;

require './bin/dbc';

local $SIG{__DIE__} = sub { confess @_ };

my $tempfile = Mojo::File->new(tempfile);
Mojo::File->new('t/testdata/dbc.conf')->copy_to($tempfile);

local $ENV{APP_DBC_CONFIGFILE} = $tempfile;

{    # Test all param iterators for commandline parameters
  my @tests = (

    # Auto
    [ '_make_param_iterator_for_auto_array',
      'SELECT ? AS c1, ? AS c2, ? AS c3',
      [ 'a1|a2|a3', '--informat=auto', '--outformat=yaml' ],
      "---\nc1: a1\nc2: a2\nc3: a3\n"
    ],
    [ '_make_param_iterator_for_auto_array looping',
      'SELECT ? AS c1, ? AS c2, ? AS c3',
      [ 'a1|a2|a3',        'b1|b2|b3',
        '--informat=auto', '--outformat=yaml',
        '--loop'
      ],
      "---\nc1: a1\nc2: a2\nc3: a3\n---\nc1: b1\nc2: b2\nc3: b3\n"
    ],
    [ '_make_param_iterator_for_auto_array single element',
      'SELECT ? AS c1, ? AS c2',
      [ 'a1', 'b1', '--informat=auto', '--outformat=yaml' ],
      "---\nc1: a1\nc2: b1\n"
    ],
    [ '_make_param_iterator_for_auto_array single element looping',
      'SELECT ? AS c1',
      [ 'a1', 'b1', '--informat=auto', '--outformat=yaml', '--loop' ],
      "---\nc1: a1\n---\nc1: b1\n"
    ],
    [ '_make_param_iterator_for_auto_array magic spells',
      "SELECT /*<KEYS=1>*/",
      [ 'a1|a2|a3', '--informat=auto', '--outformat=yaml' ],
      "---\n'?': a2\n"
    ],
    [ '_make_param_iterator_for_auto_array magic spells looping',
      'SELECT /*<KEYS=0>*/',
      [ 'a1|a2|a3',        'b1|b2|b3',
        '--informat=auto', '--outformat=yaml',
        '--loop'
      ],
      "---\n'?': a1\n---\n'?': b1\n"
    ],
    [ '_make_param_iterator_for_auto_array magic spells skip header',
      'SELECT /*<KEYS=c3>*/',
      [ 'c1|c2|c3',        'a1|a2|a3',
        '--informat=auto', '--outformat=yaml',
        '--skip-input-header'
      ],
      "---\n'?': a3\n"
    ],
    [ '_make_param_iterator_for_auto_array magic spells skip header looping',
      'SELECT /*<KEYS=c3>*/',
      [ 'c1|c2|c3',         'a1|a2|a3',
        'b1|b2|b3',         '--informat=auto',
        '--outformat=yaml', '--skip-input-header',
        '--loop'
      ],
      "---\n'?': a3\n---\n'?': b3\n"
    ],

    # CSV
    [ '_make_param_iterator_for_csv_array',
      'SELECT ? AS c1, ? AS c2, ? AS c3',
      [ 'a1,a2,a3', '--informat=csv', '--outformat=yaml' ],
      "---\nc1: a1\nc2: a2\nc3: a3\n"
    ],
    [ '_make_param_iterator_for_csv_array looping',
      'SELECT ? AS c1, ? AS c2, ? AS c3',
      [ 'a1,a2,a3',       'b1,b2,b3',
        '--informat=csv', '--outformat=yaml',
        '--loop'
      ],
      "---\nc1: a1\nc2: a2\nc3: a3\n---\nc1: b1\nc2: b2\nc3: b3\n"
    ],
    [ '_make_param_iterator_for_csv_array single element',
      'SELECT ? AS c1, ? AS c2',
      [ 'a1', 'b1', '--informat=csv', '--outformat=yaml' ],
      "---\nc1: a1\nc2: b1\n"
    ],
    [ '_make_param_iterator_for_csv_array single element looping',
      'SELECT ? AS c1',
      [ 'a1', 'b1', '--informat=csv', '--outformat=yaml', '--loop' ],
      "---\nc1: a1\n---\nc1: b1\n"
    ],
    [ '_make_param_iterator_for_csv_array magic spells',
      "SELECT /*<KEYS=1>*/",
      [ 'a1,a2,a3', '--informat=csv', '--outformat=yaml' ],
      "---\n'?': a2\n"
    ],
    [ '_make_param_iterator_for_csv_array magic spells looping',
      'SELECT /*<KEYS=0>*/',
      [ 'a1,a2,a3',       'b1,b2,b3',
        '--informat=csv', '--outformat=yaml',
        '--loop'
      ],
      "---\n'?': a1\n---\n'?': b1\n"
    ],
    [ '_make_param_iterator_for_csv_array magic spells skip header',
      'SELECT /*<KEYS=c3>*/',
      [ 'c1,c2,c3',       'a1,a2,a3',
        '--informat=csv', '--outformat=yaml',
        '--skip-input-header'
      ],
      "---\n'?': a3\n"
    ],
  );

  foreach my $test (@tests) {
    my ( $testname, $query, $params, $expected ) = @{$test};

    my $stdout_data = '';
    open( my $stdout, '>', \$stdout_data ) or die $!;
    local *STDOUT = $stdout;

    is App::dbc::main( 'mem', $query, @{$params} ), App::dbc::ZERO_BUT_TRUE(),
        "$testname RC";
    is_deeply $stdout_data, $expected, "$testname data";
  }
}

{    # Test all param iterators for commandline(STDIN) parameters with filehandle params
  my @tests = (

    [ '_make_param_iterator_for_auto_fh',
      "a1|a2|a3\n",
      "SELECT ? AS c1, ? AS c2, ? AS c3",
      [ '--informat=auto', '--outformat=yaml' ],
      "---\nc1: a1\nc2: a2\nc3: a3\n"
    ],
    [ '_make_param_iterator_for_auto_fh looping',
      "a1|a2|a3\n\nb1|b2|b3\n",
      "SELECT ? AS c1, ? AS c2, ? AS c3",
      [ '--informat=auto', '--outformat=yaml', '--loop' ],
      "---\nc1: a1\nc2: a2\nc3: a3\n---\nc1: b1\nc2: b2\nc3: b3\n"
    ],
    [ '_make_param_iterator_for_auto_fh single element',
      "a1\n\nb1\n",
      "SELECT ? AS c1, ? AS c2",
      [ '--informat=auto', '--outformat=yaml' ],
      "---\nc1: a1\nc2: b1\n"
    ],
    [ '_make_param_iterator_for_auto_fh single element looping',
      "a1\n\nb1\n",
      "SELECT ? AS c1",
      [ '--informat=auto', '--outformat=yaml', '--loop' ],
      "---\nc1: a1\n---\nc1: b1\n"
    ],
    [ '_make_param_iterator_for_auto_fh magic spells',
      "a1|a2|a3\n",
      "SELECT /*<KEYS=1>*/",
      [ '--informat=auto', '--outformat=yaml' ],
      "---\n'?': a2\n"
    ],
    [ '_make_param_iterator_for_auto_fh magic spells looping',
      "a1|a2|a3\nb1|b2|b3\n",
      "SELECT /*<KEYS=0>*/",
      [ '--informat=auto', '--outformat=yaml', '--loop' ],
      "---\n'?': a1\n---\n'?': b1\n"
    ],
    [ '_make_param_iterator_for_auto_fh magic spells skip header',
      "c1|c2|c3\na1|a2|a3\n",
      "SELECT /*<KEYS=c3>*/",
      [ '--informat=auto', '--outformat=yaml', '--skip-input-header' ],
      "---\n'?': a3\n"
    ],

    # CSV
    [ '_make_param_iterator_for_csv_fh',
      "a1,a2,a3\n",
      "SELECT ? AS c1, ? AS c2, ? AS c3",
      [ '--informat=csv', '--outformat=yaml' ],
      "---\nc1: a1\nc2: a2\nc3: a3\n"
    ],
    [ '_make_param_iterator_for_csv_fh looping',
      "a1,a2,a3\nb1,b2,b3\n",
      "SELECT ? AS c1, ? AS c2, ? AS c3",
      [ '--informat=csv', '--outformat=yaml', '--loop' ],
      "---\nc1: a1\nc2: a2\nc3: a3\n---\nc1: b1\nc2: b2\nc3: b3\n"
    ],
    [ '_make_param_iterator_for_csv_fh single element',
      "a1\nb1\n",
      "SELECT ? AS c1, ? AS c2",
      [ '--informat=csv', '--outformat=yaml' ],
      "---\nc1: a1\nc2: b1\n"
    ],
    [ '_make_param_iterator_for_csv_fh single element looping',
      "a1\nb1\n",
      "SELECT ? AS c1",
      [ '--informat=csv', '--outformat=yaml', '--loop' ],
      "---\nc1: a1\n---\nc1: b1\n"
    ],
    [ '_make_param_iterator_for_csv_fh magic spells',
      "a1,a2,a3\n",
      "SELECT /*<KEYS=1>*/",
      [ '--informat=csv', '--outformat=yaml' ],
      "---\n'?': a2\n"
    ],
    [ '_make_param_iterator_for_csv_fh magic spells looping',
      "a1,a2,a3\nb1,b2,b3\n",
      "SELECT /*<KEYS=0>*/",
      [ '--informat=csv', '--outformat=yaml', '--loop' ],
      "---\n'?': a1\n---\n'?': b1\n"
    ],
    [ '_make_param_iterator_for_csv_fh magic spells skip header',
      "c1,c2,c3\na1,a2,a3\n",
      "SELECT /*<KEYS=c3>*/",
      [ '--informat=csv', '--outformat=yaml', '--skip-input-header' ],
      "---\n'?': a3\n"
    ],
  );

  foreach my $test (@tests) {
    my ( $testname, $stdin_data, $query, $params, $expected ) = @{$test};

    open( my $stdin, '<', \$stdin_data ) or die $!;
    local *STDIN = $stdin;

    my $stdout_data = '';
    open( my $stdout, '>', \$stdout_data ) or die $!;
    local *STDOUT = $stdout;

    is App::dbc::main( 'mem', "--optfile=STDIN", $query, @{$params} ),
        App::dbc::ZERO_BUT_TRUE(), "$testname RC";
    is_deeply $stdout_data, $expected, "$testname data";
  }
}

{    # Test all sql iterators
  my @tests = (
    [ "single query", "SELECT DATETIME(0, 'unixepoch') AS TIME\n",
      [],             qq[{"TIME":"1970-01-01 00:00:00"}\n]
    ],
    [ "single query with one param", "SELECT ? AS c1\n",
      ['a1'],                        qq[{"c1":"a1"}\n]
    ],
    [ "single query with two params",
      "SELECT ? AS c1, ? AS c2\n",
      [ 'a1', 'b1' ],
      qq[{"c1":"a1","c2":"b1"}\n]
    ],
    [ "single query with one param set", "SELECT ? AS c1, ? AS c2\n",
      ['a1|b1'],                         qq[{"c1":"a1","c2":"b1"}\n]
    ],
    [ "single query with two param sets loop",
      "SELECT ? AS c1, ? AS c2\n",
      [ 'a1|b1', 'a2|b2', '--loop' ],
      qq[{"c1":"a1","c2":"b1"}\n{"c1":"a2","c2":"b2"}\n]
    ],
    [ "multi query", "SELECT 1 AS NUM;\nSELECT 2 AS NUM;",
      [],            qq[{"NUM":1}\n{"NUM":2}\n]
    ],
    [ "multi query with one param", "SELECT ? AS c1;\nSELECT ? AS q1",
      ['a1'],                       qq[{"c1":"a1"}\n{"q1":"a1"}\n]
    ],
    [ "multi query with two params",
      "SELECT ? AS c1, ? AS c2;\nSELECT ? AS q1, ? AS q2",
      [ 'a1', 'b1' ],
      qq[{"c1":"a1","c2":"b1"}\n{"q1":"a1","q2":"b1"}\n]
    ],
    [ "multi query with one param set",
      "SELECT ? AS c1, ? AS c2;\nSELECT ? AS q1, ? AS q2",
      ['a1|b1'],
      qq[{"c1":"a1","c2":"b1"}\n{"q1":"a1","q2":"b1"}\n]
    ],
    [ "multi query with two param sets loop",
      "SELECT ? AS c1, ? AS c2;\nSELECT ? AS q1, ? AS q2",
      [ 'a1|b1', 'a2|b2', '--loop' ],
      qq[{"c1":"a1","c2":"b1"}\n{"c1":"a2","c2":"b2"}\n{"q1":"a1","q2":"b1"}\n{"q1":"a2","q2":"b2"}\n]
    ],
  );

  foreach my $test (@tests) {
    my ( $testname, $stdin_data, $params, $expected ) = @{$test};

    open( my $stdin, '<', \$stdin_data ) or die $!;
    local *STDIN = $stdin;

    my $stdout_data = '';
    open( my $stdout, '>', \$stdout_data ) or die $!;
    local *STDOUT = $stdout;

    is App::dbc::main( 'mem', '--sqlfile=STDIN', '--outformat=json',
      @{$params} ),
        App::dbc::ZERO_BUT_TRUE(), "$testname RC";
    is_deeply $stdout_data, $expected, "$testname data";
  }
}

{    # Test all print formats

  my @tests = (
    [ "yaml with headers",
      'yaml', "SELECT DATETIME(0, 'unixepoch') AS TIME\n",
      [],     qq[---\nTIME: 1970-01-01 00:00:00\n]
    ],
    [ "yaml without headers",
      'yaml',
      "SELECT DATETIME(0, 'unixepoch') AS TIME\n",
      ['--skip-output-header'],
      qq[---\n- 1970-01-01 00:00:00\n]
    ],
    [ "json with headers",
      'json', "SELECT DATETIME(0, 'unixepoch') AS TIME\n",
      [],     qq[{"TIME":"1970-01-01 00:00:00"}\n]
    ],
    [ "json without headers",
      'json',
      "SELECT DATETIME(0, 'unixepoch') AS TIME\n",
      ['--skip-output-header'],
      qq{["1970-01-01 00:00:00"]\n}
    ],
    [ "csv with headers",
      'csv', "SELECT DATETIME(0, 'unixepoch') AS TIME\n",
      [],    qq[TIME\n'1970-01-01 00:00:00'\n]
    ],
    [ "csv without headers",
      'csv',                    "SELECT DATETIME(0, 'unixepoch') AS TIME\n",
      ['--skip-output-header'], qq['1970-01-01 00:00:00'\n]
    ],
    [ "table string",
      'table',
      join( "\n",
        q{          SELECT DATETIME(0, 'unixepoch') AS TIME,  NULL AS ''},
        q{UNION ALL SELECT DATETIME(1, 'unixepoch') AS TIME,   'X' AS ''},
        q{UNION ALL SELECT DATETIME(2, 'unixepoch') AS TIME,     1 AS ''},
        q{UNION ALL SELECT DATETIME(3, 'unixepoch') AS TIME,    .1 AS ''},
        q{UNION ALL SELECT DATETIME(4, 'unixepoch') AS TIME,   1.1 AS ''},
        q{UNION ALL SELECT DATETIME(5, 'unixepoch') AS TIME, '1,2' AS ''},
      ),
      [],
      join( "\n",
        '+---------------------+-----+',
        '| TIME                |     |',
        '+---------------------+-----+',
        '| 1970-01-01 00:00:00 |     |',
        '| 1970-01-01 00:00:01 | X   |',
        '| 1970-01-01 00:00:02 | 1   |',
        '| 1970-01-01 00:00:03 | 0.1 |',
        '| 1970-01-01 00:00:04 | 1.1 |',
        '| 1970-01-01 00:00:05 | 1,2 |',
        '+---------------------+-----+',
        '' )
    ],
    [ "table numeric leading 0",
      'table',
      join( "\n",
        q{          SELECT '00001' AS str},
        q{UNION ALL SELECT '002' AS str},
      ),
      [],
      join( "\n",
        '+-------+', '| str   |', '+-------+', '| 00001 |',
        '| 002   |', '+-------+', '' )
    ],
    [ "table numeric",
      'table',
      join( "\n",
        q{          SELECT  .1 AS num},
        q{UNION ALL SELECT   1 AS num},
        q{UNION ALL SELECT 1.0 AS num},
        q{UNION ALL SELECT 2.1 AS num},
      ),
      [],
      join( "\n",
        '+-----+', '| num |', '+-----+', '| 0.1 |', '|   1 |',
        '|   1 |', '| 2.1 |', '+-----+', '' )
    ],
    [ "table without headers",
      'table',
      "SELECT DATETIME(0, 'unixepoch') AS TIME, 'c2'\n",
      ['--skip-output-header'],
      qq[+---------------------+----+\n]
          . qq[| 1970-01-01 00:00:00 | c2 |\n]
          . qq[+---------------------+----+\n]
    ],
    [ 'vertical',
      'vertical',
      q{SELECT DATETIME(0, 'unixepoch') AS TIME, 'X' AS str, 'Y' AS abc},
      [],
      qq{--------- 1 / 1 ----------\n TIME: 1970-01-01 00:00:00\n  str: X\n  abc: Y\n}
    ],
    [ 'vertical without headers',
      'vertical',
      q{SELECT DATETIME(0, 'unixepoch') AS TIME, 'X' AS str, 'Y' AS abc},
      ['--skip-output-header'],
      qq{------ 1 / 1 ------\n1970-01-01 00:00:00\nX\nY\n}
    ],
    [ 'value without headers',
      'value',                  q{SELECT DATETIME(0, 'unixepoch') AS TIME},
      ['--skip-output-header'], qq{1970-01-01 00:00:00}
    ],
    [ 'column with headers',
      'column',
      q{SELECT 33 AS A, 4 AS B UNION ALL SELECT -8.1 AS l, 12201 AS c},
      [],
      join( "\n",
        qq{+-+----+-------+}, qq{ A: 33 |  -8.1 |},
        qq{ B:  4 | 12201 |}, qq{+-+----+-------+\n} )
    ],
    [ 'column without headers',
      'column',
      q{SELECT NULL AS A, 4 AS B UNION ALL SELECT -8.1 AS l, 12201 AS c},
      ['--skip-output-header'],
      join( "\n", qq{   |  -8.1}, qq{ 4 | 12201\n} )
    ],
    [ 'auto empty result',
      'auto',
      q{SELECT 1 WHERE 1=0},
      [],
      qq{No rows matched\n}
    ],
    [ 'auto',
      'auto',
      q{          SELECT DATETIME(0, 'unixepoch') AS TIME, 1 AS num},
      ['--vertical=1'],
      qq{+----+---------------------+\n}
          . qq{ TIME: 1970-01-01 00:00:00 |\n}
          . qq{  num: 1                   |\n}
          . qq{+----+---------------------+\n}
    ],
    [ 'auto without headers',
      'auto',
      q{          SELECT DATETIME(0, 'unixepoch') AS TIME, 1 AS num},
      ['--skip-output-header'],
      qq{ 1970-01-01 00:00:00\n 1                  \n}
    ],
    [ 'auto', 'auto',
      join( "\n",
        q{          SELECT DATETIME(0, 'unixepoch') AS TIME, 1 AS num},
        q{UNION ALL SELECT DATETIME(1, 'unixepoch') AS TIME, 2 AS num},
      ),
      ['--vertical=1'],
      join( "\n",
        q{+---------------------+-----+}, q{| TIME                | num |},
        q{+---------------------+-----+}, q{| 1970-01-01 00:00:00 |   1 |},
        q{| 1970-01-01 00:00:01 |   2 |}, q{+---------------------+-----+},
        q{} )
    ],
    [ 'msgpack',
      'msgpack',
      join( "\n",
        q{          SELECT DATETIME(0, 'unixepoch') AS TIME, 1 AS num},
        q{UNION ALL SELECT DATETIME(1, 'unixepoch') AS TIME, 2 AS num},
      ),
      [],
      pack( 'H*',
        join '',
        '92c40454494d45c4036e756dc192c413313937302d30312d30312030303a',
        '30303a303001c192c413313937302d30312d30312030303a30303a303102',
        'c1' )
    ],
    [ 'msgpack without headers',
      'msgpack',
      join( "\n",
        q{          SELECT DATETIME(0, 'unixepoch') AS TIME, 1 AS num},
        q{UNION ALL SELECT DATETIME(1, 'unixepoch') AS TIME, 2 AS num},
      ),
      [],
      pack( 'H*',
        join '',
        '92c40454494d45c4036e756dc192c413313937302d30312d30312030303a',
        '30303a303001c192c413313937302d30312d30312030303a30303a303102',
        'c1' )
    ],
    [ 'msgpack-kv',
      'msgpack-kv',
      join( "\n",
        q{          SELECT DATETIME(0, 'unixepoch') AS TIME, 1 AS num},
        q{UNION ALL SELECT DATETIME(1, 'unixepoch') AS TIME, 2 AS num},
      ),
      [],
      pack( 'H*',
        join '',
        '82c40454494d45c413313937302d30312d30312030303a30303a3030c403',
        '6e756d01c182c40454494d45c413313937302d30312d30312030303a3030',
        '3a3031c4036e756d02c1' )
    ],
    [ 'msgpack-kv without headers',
      'msgpack-kv',
      join( "\n",
        q{          SELECT DATETIME(0, 'unixepoch') AS TIME, 1 AS num},
        q{UNION ALL SELECT DATETIME(1, 'unixepoch') AS TIME, 2 AS num},
      ),
      [],
      pack( 'H*',
        join '',
        '82c40454494d45c413313937302d30312d30312030303a30303a3030c403',
        '6e756d01c182c40454494d45c413313937302d30312d30312030303a3030',
        '3a3031c4036e756d02c1' )
    ],
    [ 'graph with headers',
      'graph',
      join( "\n",
        q{          SELECT 'A' AS col, 1 AS num},
        q{UNION ALL SELECT 'B' AS col, 2 AS num},
        q{UNION ALL SELECT 'C' AS col, 3 AS num},
        q{UNION ALL SELECT 'D' AS col, 1 AS num},
      ),
      [],
      pack( 'H*',
        join '',
        '206d696e3a20302c206d61783a2033200a7c202020202020202020202020',
        '7c0a7c2020202020202020202020207c0a7c202020202020202020202020',
        '7c0a7c2020202020202020202020207c0a7c202020202020202020202020',
        '7c0a7c2020202020202020202020207c0a7c202020202020202020202020',
        '7c0a7c2020202020202020202020207c0a7c2020202020202a2020202020',
        '7c0a7c2020202a20202a20202020207c0a7c2a20202a20202a20202a2020',
        '7c0a7c2a20202a20202a20202a20207c0a7ce28094e28094e28094e28094',
        'e28094e28094e28094e28094e28094e28094e28094e280947c0a' )
    ],
    [ 'graph without headers',
      'graph',
      join( "\n",
        q{          SELECT 'A' AS col, 1 AS num},
        q{UNION ALL SELECT 'B' AS col, 2 AS num},
        q{UNION ALL SELECT 'C' AS col, 3 AS num},
        q{UNION ALL SELECT 'D' AS col, 1 AS num},
      ),
      [],
      pack( 'H*',
        join '',
        '206d696e3a20302c206d61783a2033200a7c202020202020202020202020',
        '7c0a7c2020202020202020202020207c0a7c202020202020202020202020',
        '7c0a7c2020202020202020202020207c0a7c202020202020202020202020',
        '7c0a7c2020202020202020202020207c0a7c202020202020202020202020',
        '7c0a7c2020202020202020202020207c0a7c2020202020202a2020202020',
        '7c0a7c2020202a20202a20202020207c0a7c2a20202a20202a20202a2020',
        '7c0a7c2a20202a20202a20202a20207c0a7ce28094e28094e28094e28094',
        'e28094e28094e28094e28094e28094e28094e28094e280947c0a' )
    ],
  );

  my @formats_to_test = sort keys %{ App::dbc::OUTPUT_FUNCTIONS() };
  my @formats_tested  = uniq sort map { $_->[1] } @tests;
  is_deeply \@formats_tested, \@formats_to_test,
      'verify all output formats have a test';

  my @formats_tested_wo_headers =
      uniq sort map { $_->[0] } grep { $_->[0] =~ /without headers$/ } @tests;
  is_deeply \@formats_tested_wo_headers,
      [ map {"$_ without headers"} @formats_to_test ],
      'verify all output formats have a test without headers';

  foreach my $test (@tests) {
    my ( $testname, $format, $query, $params, $expected ) = @{$test};

    no warnings;
    local *App::dbc::_terminal_cols_and_rows = sub { return ( 98, 15 ) };
    use warnings;

    my $stdout_data = '';
    open( my $stdout, '>', \$stdout_data ) or die $!;
    local *STDOUT = $stdout;

    is App::dbc::main( 'mem', $query, "--outformat=$format", @{$params} ),
        App::dbc::ZERO_BUT_TRUE(),
        "$testname RC";
    is_deeply $stdout_data, $expected, "$testname data";
  }

  {    # Abort limit
    my $query = q{ SELECT DATETIME(0, 'unixepoch') AS TIME, 1 AS num
         UNION ALL SELECT DATETIME(1, 'unixepoch') AS TIME, 2 AS num
    };

    my $stdin_data = "i\n";

    open( my $stdin, '<', \$stdin_data ) or die $!;
    local *STDIN = $stdin;

    my $stdout_data = '';
    open( my $stdout, '>', \$stdout_data ) or die $!;
    local *STDOUT = $stdout;

    App::dbc::main( 'mem', $query, '--outformat=auto', '--vertical=1',
      '--abort=1' );
    like $stdout_data, qr/The query returned a dataset larger than 1 rows/;
  }
}

{    # Time range looping
  my @tests = (

    # Time intervals
    [ 'table',
      q{SELECT ? AS "FROM", ? "UPTO"},
      [ q{--from=2020-01-01 01:00:00},
        q{--upto=2020-01-07 07:00:00},
        q{--by=day}
      ]
    ],
    [ 'table',
      q{SELECT ? AS 'FROM', ? 'UPTO'},
      [ q{--from=2020-01-01 01:00:00},
        q{--upto=2020-01-01 07:00:00},
        q{--by=hour}
      ]
    ],
    [ 'table',
      q{SELECT ? AS 'FROM', ? 'UPTO'},
      [ q{--from=2020-01-01 01:00:00},
        q{--upto=2020-01-01 02:00:00},
        q{--by=minute}
      ]
    ],
    [ 'table',
      q{SELECT ? AS 'FROM', ? 'UPTO'},
      [ q{--from=2020-01-01 01:01:58},
        q{--upto=2020-01-01 01:03:02},
        q{--by=second}
      ]
    ],
    [ 'column',
      q{SELECT ? AS 'FROM', ? 'UPTO'},
      [ q{--from=2023-11-01}, q{--upto=2023-11-04},
        q{--by=day},          q{--reverse}
      ]
    ],
    [ 'column',
      q{SELECT ? AS 'FROM', ? 'UPTO'},
      [ q{--from=2023-11-01}, q{--upto=2023-11-04}, q{--by=day} ]
    ],
    [ 'column',
      q{SELECT ? AS 'FROM', ? 'UPTO'},
      [ q{--from=10}, q{--upto=20}, q{--by=2} ]
    ],
    [ 'column',
      q{SELECT ? AS 'FROM', ? 'UPTO'},
      [ q{--from=10}, q{--upto=20}, q{--by=2}, q{--reverse} ]
    ]
  );

  my $tempfile = Mojo::File->new(tempfile);
  my $temppath = $tempfile->to_string;

  my $i = {};
  foreach my $test (@tests) {

    my ( $format, $query, $params ) = @{$test};
    my $num = $i->{$format} += 1;

    my $filename = "t/testdata/out/$format-$num.txt";

    is App::dbc::main( 'mem', "--outformat=$format", $query, @{$params},
      "--outfile=$temppath" ),
        App::dbc::ZERO_BUT_TRUE(), "main -> $format rc OK"
        or diag Dumper($test);

    is $tempfile->slurp, Mojo::File->new($filename)->slurp,
        "main -> $format-$num content OK"
        or diag Dumper($test);
  }
}

{    # auto function include dialogue
  my @tests = (

    # File input
    [ 'auto', '--sqlfile=t/testdata/select-single.sql',
      [ '--loop', qw(n1 n2 n3) ]
    ],
    [ 'auto',
      '--sqlfile=t/testdata/select-single.sql',
      [ '--loop', '--optfile=t/testdata/params.txt' ]
    ],
    [ 'auto', '--sqlfile=t/testdata/select-multi.sql', [] ],
  );

  my $tempfile = Mojo::File->new(tempfile);
  my $temppath = $tempfile->to_string;

  my $i = {};
  foreach my $test (@tests) {
    my ( $format, $query, $params ) = @{$test};
    my $num = $i->{$format} += 1;

    my $filename = "t/testdata/out/$format-$num.txt";

    is App::dbc::main( 'mem', "--outformat=$format", $query, @{$params},
      "--outfile=$temppath" ),
        App::dbc::ZERO_BUT_TRUE(), "main -> $format rc OK"
        or diag Dumper($test);

    is $tempfile->slurp, Mojo::File->new($filename)->slurp,
        "main -> $format-$num content OK"
        or diag Dumper($test);
  }

  {
    my @params = (
      'mem', "--outformat=auto",
      '--sqlfile=t/testdata/select-single-multi-rows.sql',
      '--vertical=1', '--abort=1'
    );

    {
      my ( $rc, $stdout ) = _run_params( "\n\nq\n", \@params );

      # Leading \n\n is intentional to cover empty reply code path

      is $rc, App::dbc::ZERO_BUT_TRUE(), 'auto - dialogue q';

      like $stdout, qr/dump result to file\s+\Z/,
          'no extra output when quitting';
    }

    {
      my ( $rc, $stdout ) = _run_params( "i\n", \@params );

      is $rc, App::dbc::ZERO_BUT_TRUE(), 'auto - dialogue i';

      like $stdout, qr/\|  5 \|  6 \|/, 'table output when ignoring warning';
    }

    {
      my $tempfile = Mojo::File->new(tempfile);
      my $temppath = $tempfile->to_string;
      ok $tempfile->remove, 'remove tempfile so test can use path';

      my ( $rc, $stdout ) = _run_params( "f\n$temppath\n", \@params );

      is $rc, App::dbc::ZERO_BUT_TRUE(), 'auto - dialogue f';
      like $stdout, qr/Data written to $temppath/,
          'write file path when choosing file output';
      ok $tempfile->remove, 'cleanup testfile';
    }
  }
}

{    # graph function
  no warnings;
  local *App::dbc::_terminal_cols_and_rows = sub { return ( 98, 14 ) };
  use warnings;

  my @params = (
    'mem', '--outformat=graph', '--sqlfile=t/testdata/sqlite-recursive.sql'
  );

  my ( $rc, $stdout ) = _run_params( "\n", \@params );
  is $rc, '0 but true', 'rc OK';
  my $expected = Mojo::File->new('t/testdata/out/graph.txt')->slurp;

  # Ignore leading terminal manipulation to clear screen
  is substr( $stdout, -length($expected) ), $expected,
      'sqlite-recursive.sql graph is OK';
}

{    # on_connect_execute
  my ( $rc, $text ) = _run_params( "9\n",
    [ 'auto', 'select ? as C', '--outformat=auto', '--optfile=STDIN' ] );
  is $rc,   App::dbc::ZERO_BUT_TRUE(),    'on_connect_execute rc';
  is $text, "- 1 / 1 -\n A: 1\n B: 2\n9", 'on_connect_execute text';
}

{    # verbose 2
  my $org_query_type = \&App::dbc::_resolve_query_type;

  no warnings;
  local *App::dbc::_resolve_query_type =
      sub { sleep 1; $org_query_type->(@_) };
  use warnings;

  my ( $rc, $text ) = _run_params(
    "1\n",
    [ 'mem',                  'select ? as A',
      '--outformat=vertical', '--verbose=2',
      '--prompt-to-exit',     '1',
      '--optfile=STDIN'
    ]
  );
  is $rc, App::dbc::ZERO_BUT_TRUE(), 'verbose = 2 rc';
  like $text, qr/Total runtime .*? \d+([.]\d+)? seconds?/, 'verbose = 2 text';
  like $text, qr/click <enter> to exit/, 'click <enter> to exit triggered';
}

{    # auto -> column
     # Verify column mode is used when the sceen can accomodate it

  my @params = (
    'mem',    q{select ? AS 'from', ? AS 'upto'},
    '--from', '2020-01-01',
    '--upto', '2020-01-04', '--by', 'day', '--outformat', 'auto'
  );

  {
    no warnings;
    local *App::dbc::_terminal_cols_and_rows = sub { return ( 250, 250 ) };
    use warnings;

    my $stdout_data = '';
    open( my $stdout, '>', \$stdout_data ) or die $!;
    local *STDOUT = $stdout;

    is App::dbc::main(@params), App::dbc::ZERO_BUT_TRUE(), 'rc OK';

    my $expected = join "\n",
        q{+----+---------------------+---------------------+---------------------+},
        q{ from: 2020-01-01 00:00:00 | 2020-01-02 00:00:00 | 2020-01-03 00:00:00 |},
        q{ upto: 2020-01-02 00:00:00 | 2020-01-03 00:00:00 | 2020-01-04 00:00:00 |},
        q{+----+---------------------+---------------------+---------------------+}
        . "\n";

    is $stdout_data, $expected, 'text OK';
  }

  {
    no warnings;
    local *App::dbc::_terminal_cols_and_rows = sub { return ( 50, 50 ) };
    use warnings;

    my $stdout_data = '';
    open( my $stdout, '>', \$stdout_data ) or die $!;
    local *STDOUT = $stdout;

    is App::dbc::main(@params), App::dbc::ZERO_BUT_TRUE(), 'rc OK';

    my $expected = join "\n",
        q{--------- 1 / 3 ----------},
        q{ from: 2020-01-01 00:00:00},
        q{ upto: 2020-01-02 00:00:00},
        q{--------- 2 / 3 ----------},
        q{ from: 2020-01-02 00:00:00},
        q{ upto: 2020-01-03 00:00:00},
        q{--------- 3 / 3 ----------},
        q{ from: 2020-01-03 00:00:00},
        q{ upto: 2020-01-04 00:00:00} . "\n";

    is $stdout_data, $expected, 'text OK';
  }
}

{
  throws_ok {
    App::dbc::main(
      'mem',       'SELECT ? AS n1, ? AS n2',
      '--from=20', '--upto=10',
      '--by=day'
    )
  }
  qr/Error parsing time/, 'cannot mix date and int param types';

  throws_ok {
    App::dbc::main(
      'mem',       'SELECT ? AS n1, ? AS n2',
      '--from=20', '--upto=10',
      '--by=1'
    )
  }
  qr/upto must be greater then from at/, 'from is less then opto';

  throws_ok {
    App::dbc::main(
      'mem',               'SELECT ? AS d1, ? AS d2',
      '--from=2021-01-01', '--upto=2020-01-01',
      '--by=day'
    )
  }
  qr/upto must be greater then from at/, 'from is less then opto';

  throws_ok {
    App::dbc::main( 'mem', 'SELECT ? AS n1',
      '--from=2010-01-01', '--upto=2020-01-01' )
  }
  qr/from, upto and by must all be provided or none/, '--by has not been set';

  throws_ok {
    App::dbc::main(
      'mem',               'SELECT ? AS n1',
      'z1',                '--from=2010-01-01',
      '--upto=2020-01-01', '--by=invalid'
    )
  }
  qr/invalid by option: invalid/, 'invalid --by cause error';

  throws_ok {
    App::dbc::main(
      'mem',               'SELECT ? AS n1',
      '--from=2010-01-01', '--upto=2020-01-01',
      '--by=hour',         '--optfile=somefile.txt'
    )
  }
  qr/only one source of parameters can be used at the same time/,
      'cannot use both time loop and optfile at the same time';

  throws_ok {
    App::dbc::main(
      'mem',             'SELECT ? AS n1, ? AS n2, ? AS n3',
      '--optfile=STDIN', '--informat=invalid',
      '--loop'
    )
  }
  qr/invalid informat: invalid/, 'invalid --informat cause error';

  throws_ok { App::dbc::main( 'mem', '--outformat=invalid' ) }
  qr/invalid outformat: invalid/;

  throws_ok {
    App::dbc::main( 'mem', '--optfile=STDIN', '--sqlfile=STDIN' )
  }
  qr/optfile and sqlfile cannot both be STDIN/;

  throws_ok { App::dbc::main( 'mem', '--optfile=STDIN', '--sqlfile=STDIN' ) }
  qr/optfile and sqlfile cannot both be STDIN/;

  throws_ok { App::dbc::main( 'mem', 'UNKNOWN QUERY TYPE' ) }
  qr/(?:execute|prepare) failed/;

  throws_ok {
    App::dbc::main( 'mem', 'UNKNOWN QUERY TYPE', '--query-type=SELECT' )
  }
  qr/(?:execute|prepare) failed/;

  throws_ok {
    App::dbc::main( 'mem', 'UNKNOWN QUERY TYPE', '--query-type=select' )
  }
  qr/(?:execute|prepare) failed/;

  dies_ok {
    App::dbc::main( 'mem', 'UNKNOWN QUERY TYPE', '--invalid!' )
  }
  'Unknown option: invalid';

  dies_ok {
    App::dbc::main( 'mem', 'SELECT 1 FROM does not exist!' )
  }
  'Invalid query';
}

done_testing;

sub _run_params {
  my ( $stdin_data, $app_param ) = @_;

  my $stdin_var = $stdin_data;
  open( my $stdin, '<', \$stdin_var ) or die $!;
  local *STDIN = $stdin;

  my $stdout_var = '';
  open( my $stdout, '>', \$stdout_var ) or die $!;
  local *STDOUT = $stdout;
  my $rc = App::dbc::main( @{$app_param} );

  return $rc, $stdout_var;
}

__END__
