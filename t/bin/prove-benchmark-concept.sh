#!/usr/bin/env bash

set -e

docker-compose up -d benchmark mariadb oracle

while [ $(docker inspect -f {{.State.Health.Status}} app-dbc_mariadb_1) != 'healthy' ]; do
    echo "waiting for mariadb";
    sleep 5;
done;

while [ `docker inspect -f {{.State.Health.Status}} app-dbc_oracle_1` != 'healthy' ]; do
    echo "waiting for oracle";
    sleep 10;
done;

set +e
docker-compose exec -u root benchmark tc qdisc add dev eth0 root netem delay 0ms
docker-compose exec -u root mariadb   tc qdisc add dev eth0 root netem delay 0ms
docker-compose exec -u root oracle    tc qdisc add dev eth0 root netem delay 0ms
set -e

docker-compose exec -u root benchmark tc qdisc change dev eth0 root netem delay 200ms
docker-compose exec -u root mariadb   tc qdisc change dev eth0 root netem delay 200ms
docker-compose exec -u root oracle    tc qdisc change dev eth0 root netem delay 200ms

set -x
docker-compose exec benchmark bin/dbc mariadb "INSERT INTO auto_incrementing_table (val) VALUES(?)" --outformat=table --query-type=insert --loop 0 1 2 3 4 5 6 7 8 9 --verbose=2
docker-compose exec benchmark bin/dbc mariadb "INSERT INTO auto_incrementing_table (val) VALUES(?), (?), (?), (?), (?), (?), (?), (?), (?), (?)" --outformat=table --query-type=insert 0 1 2 3 4 5 6 7 8 9  --verbose=2
docker-compose exec benchmark bin/dbc mariadb "SELECT * FROM auto_incrementing_table ORDER BY id DESC LIMIT 20"

docker-compose exec benchmark bin/dbc oracle "INSERT INTO auto_incrementing_table (val) VALUES(?)" --outformat=table --query-type=insert --loop 0 1 2 3 4 5 6 7 8 9 --verbose=2
docker-compose exec benchmark bin/dbc oracle "INSERT INTO auto_incrementing_table (val) SELECT * FROM ( SELECT ? AS val FROM DUAL UNION ALL SELECT ? AS val FROM DUAL UNION ALL SELECT ? AS val FROM DUAL UNION ALL SELECT ? AS val FROM DUAL UNION ALL SELECT ? AS val FROM DUAL UNION ALL SELECT ? AS val FROM DUAL UNION ALL SELECT ? AS val FROM DUAL UNION ALL SELECT ? AS val FROM DUAL UNION ALL SELECT ? AS val FROM DUAL UNION ALL SELECT ? AS val FROM DUAL)" --outformat=table --query-type=insert 0 1 2 3 4 5 6 7 8 9  --verbose=2
docker-compose exec benchmark bin/dbc oracle "SELECT * FROM auto_incrementing_table ORDER BY id DESC FETCH FIRST 20 ROWS ONLY"

docker-compose down

