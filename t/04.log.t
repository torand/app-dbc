use 5.012;
use strict;
use warnings;

use Carp;
use Data::Dumper;
use Mojo::File qw(tempfile);
use Test::Most;

use lib 't/lib';
use Test::DBC;

require './bin/dbc';

local $SIG{__DIE__} = sub { confess @_ };

my $tempfile = Mojo::File->new(tempfile);
ok $tempfile->remove, 'Remove tempfile so the script can create it';
Mojo::File->new('t/testdata/dbc.conf')->copy_to($tempfile);

local $ENV{APP_DBC_CONFIGFILE} = $tempfile;

my $dbct = Test::DBC->new;

# reset log to ensure it is empty
$dbct->capture_stdout(
  sub {
    my ($stdout) = @_;

    # Make sure  logging schema is applied
    is App::dbc::main( 'logger', 'SELECT ?', 'pm', '--outformat=column' ),
        '0 but true', 'seed db';

    # Reset the logging schema completely
    is App::dbc::main( 'logger', '@log', 'reset!' ), '0 but true',
        'reset! ok';

    # Verify output
    like ${$stdout}, qr/Removed \d+ outputs/, 'removed outputs';
    like ${$stdout}, qr/Removed \d+ params/,  'removed params';
    like ${$stdout}, qr/Removed \d+ querys/,  'removed querys';
    like ${$stdout}, qr/Removed \d+ logs/,    'removed logs';
  }
);

$dbct->capture_stdout(
  sub {
    my ($stdout) = @_;

    my $query = q{
              SELECT 'l' AS t, COUNT(l.id) AS n FROM log    l
    UNION ALL SELECT 'o' AS t, COUNT(o.id) AS n FROM output o
    UNION ALL SELECT 'p' AS t, COUNT(p.id) AS n FROM param  p
    UNION ALL SELECT 'q' AS t, COUNT(q.id) AS n FROM query  q
  };

    is App::dbc::main( 'log', $query, '--outformat=column' ), '0 but true',
        'reset! ok';

    like ${$stdout},
        qr/ t: l [|] o [|] p [|] q [|]\n n: 0 [|] 0 [|] 0 [|] 0 [|]/,
        'all tables are empty';
  }
);

# Write to db
$dbct->capture_stdout(
  sub {
    my ($stdout) = @_;

    my @tests = (
      [ 'DROP   TABLE IF     EXISTS testtable', [] ],
      [ 'CREATE TABLE IF NOT EXISTS testtable (
          id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
          ts TEXT,
          vl TEXT
      )',
        []
      ],
      [ 'INSERT INTO testtable(ts,vl) VALUES(?,?)',
        [ '1234567890', "myval" ]
      ],
      [ q{SELECT id, ts, vl FROM testtable WHERE 1 != ? AND 2 != ?},
        [ '--outformat=table', 1, 2 ]
      ],
    );

    foreach my $test (@tests) {
      my ( $query, $params ) = @{$test};
      is App::dbc::main( 'logger', $query, @{$params} ), '0 but true', $query;
    }

    my $expected = $dbct->trim_indent(
      q{
    rows affected: 0
    rows affected: 0
    rows affected: 1
    +----+------------+-------+
    | id | ts         | vl    |
    +----+------------+-------+
    |  1 | 1234567890 | myval |
    +----+------------+-------+
  }
    );
    is ${$stdout}, $expected, ${$stdout};
  }
);

# @log no params
$dbct->capture_stdout(
  sub {
    my ($stdout) = @_;
    is App::dbc::main( 'logger', '@log' ), '0 but true', '@log';

    my @rows = split /\v+/, ${$stdout};
    is $rows[5], '     query: DROP   TABLE IF     EXISTS testtable',
        'drop ok';
    is $rows[13], '     query: CREATE TABLE IF NOT EXISTS testtable (',
        'create ok';
    is $rows[25], '     query: INSERT INTO testtable(ts,vl) VALUES(?,?)',
        'insert ok';
    is $rows[33],
        '     query: SELECT id, ts, vl FROM testtable WHERE 1 != ? AND 2 != ?',
        'select ok';
  }
);

$dbct->capture_stdout(
  sub {
    my ($stdout) = @_;

    # Inspect INSERT record without output param
    is App::dbc::main( 'logger', '@log' ), '0 but true', '@log';

    my ( $id_of_insert, $id_of_select ) =
        ( split /\v+/, ${$stdout} )[ 21, 29 ];

    ($id_of_insert) = $id_of_insert =~ m/id: (\d+)/;

    if ( ok $id_of_insert, 'got insert id' ) {
      is App::dbc::main( 'logger', '@log', $id_of_insert ), '0 but true',
          "\@log $id_of_insert";

      my $got      = join "\n", ( split /\v+/, ${$stdout} )[ -6 .. -1 ];
      my $expected = $dbct->trim_indent( '
      Query:
      INSERT INTO testtable(ts,vl) VALUES(?,?);
      Params:
      +------------+-------+
      | 1234567890 | myval |
      +------------+-------+'
      );
      is $got, $expected, 'insert returned query + table of param';
    }

    # Inspect SELECT record with output param
    ($id_of_select) = $id_of_select =~ m/id: (\d+)/;

    if ( ok $id_of_select, 'got select id' ) {
      is App::dbc::main( 'logger', '@log', $id_of_select, '--outformat=csv' ),
          '0 but true', "\@log $id_of_select";

      my $got      = join "\n", ( split /\v+/, ${$stdout} )[ -7 .. -1 ];
      my $expected = $dbct->trim_indent( '
      Query:
      SELECT id, ts, vl FROM testtable WHERE 1 != ? AND 2 != ?;
      Params:
      1,2
      Output:
      id,ts,vl
      1,1234567890,myval'
      );
      is $got, $expected, 'select returned query + table of param';
    }
  }
);

$dbct->capture_stdout(
  sub {
    my ($stdout) = @_;

    # Inspect INSERT record without output param
    is App::dbc::main( 'logger', '@log', 'prune', '2' ), '0 but true',
        '@log prune 2';

    is ${$stdout},
        "Removing from table log, 2 rows\nRemoving from table query, 2 rows\n",
        'removed all but 2 records';
  }
);

$dbct->capture_stdout(
  sub {
    my ($stdout) = @_;

    my $query = q{
              SELECT 'l' AS t, COUNT(l.id) AS n FROM log    l
    UNION ALL SELECT 'o' AS t, COUNT(o.id) AS n FROM output o
    UNION ALL SELECT 'p' AS t, COUNT(p.id) AS n FROM param  p
    UNION ALL SELECT 'q' AS t, COUNT(q.id) AS n FROM query  q
  };

    is App::dbc::main( 'log', $query, '--outformat=csv' ),
        '0 but true',
        'count table content';

    like ${$stdout},
        qr/t,n\nl,2\no,2\np,2\nq,2\n/,
        '2 rows left in all tables';
  }
);

done_testing;

__END__

=pod

=head2 DESCRIPTION

  Query logging is the idea of logging the query that was executed along with
  the result so it can be documented and recovered later if desired.

=head2 RATIONALE FOR IMPLEMENTATION

  While plain textfiles are very often prefeered for logging, in this case I
  opted not to do so. The reason is the data may contain binary data
  (blob fields), and multi-line content. Also due to the desire to log the
  output of the query and the query parameters in addition to the query itself,
  it makes for a rather complicated format and will not be easy to work with.

  By using a custom format we can do additional improvements like compression
  to save space and by using a database we can make it easy to query for
  statistics.

  Since this is a database querying tool we could simply build in the log
  inspection tool so it will always be available

  Currently we log to a SQLite database, up to a certain size of output.

=cut

