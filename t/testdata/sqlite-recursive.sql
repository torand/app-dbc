WITH countup AS (
            SELECT 1   AS x, 1     p
  UNION ALL SELECT x+1 AS x, 5 + COS(x) * 5 * (-10 + (x/10))
            FROM countup
            WHERE x < 120
)
SELECT * FROM countup;
