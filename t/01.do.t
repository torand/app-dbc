use 5.012;
use strict;
use warnings;

use Carp;
use Data::Dumper;
use Mojo::File qw(tempfile);
use Test::Most;

require './bin/dbc';

local $SIG{__DIE__} = sub { confess @_ };

my $tempfile = Mojo::File->new(tempfile);
ok $tempfile->remove, 'Remove tempfile so the script can create it';
Mojo::File->new('t/testdata/dbc.conf')->copy_to($tempfile);

local $ENV{APP_DBC_CONFIGFILE} = $tempfile;

{    # App::dbc::main()
  my @tests = (
    [ 'DROP TABLE IF EXISTS mytable', [ '--verbose', '0' ] ],
    [ 'CREATE TABLE mytable (
         id       INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
         inserted timestamp,
         string   varchar(128)
      )', [ '--verbose', '0' ]
    ],
    [ q{INSERT INTO mytable (id, string) VALUES(1, '000')},
      [ '--verbose', '0' ]
    ],
    [ q{INSERT INTO mytable (string) VALUES(?) },
      [ 'abc', 'def', '--loop', '--verbose', '0' ]
    ],
    [ q{INSERT INTO mytable (string) VALUES(?), (?) },
      [ 'ghi', 'jkl', '--verbose', '0' ]
    ],
  );

  foreach my $test (@tests) {
    my ( $query, $params ) = @{$test};
    is App::dbc::main( 'sqlite', $query, @{$params} ), '0 but true', $query;
  }

  {
    my $stdout_var = '';
    open( my $stdout, '>', \$stdout_var ) or die $!;
    local *STDOUT = $stdout;

    my @tests = (
      [ 'DROP TABLE IF EXISTS list_of_things ', [ '--verbose', '0' ] ],
      [ 'CREATE TABLE list_of_things (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT)',
        []
      ],
      [ 'INSERT INTO  list_of_things (NAME) VALUES(?)', ['ABC'] ],
      [ q{/*<QUERYTYPE=insert>*/ INSERT INTO list_of_things (name) VALUES(?) RETURNING *},
        ['abc']
      ],
      [ q{/*<QUERYTYPE=select>*/ INSERT INTO list_of_things (name) VALUES(?) RETURNING *},
        [ 'abc', '--outformat=table', '--query-type=insert' ]
      ],
    );

    foreach my $test (@tests) {
      my ( $query, $params ) = @{$test};
      is App::dbc::main( 'sqlite', $query, @{$params} ), '0 but true', $query;
    }

    my $expected = trim_indent( '
      rows affected: 0
      rows affected: 1
      rows affected: 0
      +----+------+
      | ID | NAME |
      +----+------+
      |  3 | abc  |
      +----+------+
    ' );
    is $stdout_var, $expected,
        'printed table for the QUERYTYPE=select statement';
  }

  {    # SQL from STDIN

    my $stdout_var = '';
    open( my $stdout, '>', \$stdout_var ) or die $!;
    local *STDOUT = $stdout;

    my $stdin_var = join ";\n",
        q{DELETE FROM mytable},
        q{INSERT INTO mytable (string) VALUES('sql 1')},
        q{INSERT INTO mytable (string) VALUES('sql 2')},
        q{INSERT INTO mytable (string) VALUES('sql 3')},
        qq{INSERT INTO\n\tmytable (string)\n\tVALUES('sql 1 with ;')},
        qq{},
        qq{  },
        qq{INSERT INTO\n\tmytable (string)\n\tVALUES('sql 2 with ;')};

    open( my $stdin, '<', \$stdin_var ) or die $!;
    local *STDIN = $stdin;

    is App::dbc::main( 'sqlite', '--sqlfile=STDIN' ),
        '0 but true',
        'read sql from STDIN';

    App::dbc::main( 'sqlite', 'SELECT id, string FROM mytable',
      '--outformat=table' );
    is $stdout_var,
        Mojo::File->new('t/testdata/out/sql-insert-stdin.txt')->slurp,
        'SQL from STDIN OK';

    ok App::dbc::main( 'sqlite', 'DELETE FROM mytable' ), 'reset mytable';
  }

  {    # SQL from STDIN CSV subselect
    my $stdout_var = '';
    open( my $stdout, '>', \$stdout_var ) or die $!;
    local *STDOUT = $stdout;

    my $stdin_var = join "\n",
        q{id,something,string},
        q{1,YYY,string1},
        q{2,XXX,string2},
        q{};

    open( my $stdin, '<', \$stdin_var ) or die $!;
    local *STDIN = $stdin;

    is App::dbc::main(
      'sqlite',
      'INSERT INTO mytable (id,string) values(/*<KEYS=id,string>*/)',
      '--informat=csv',
      '--optfile=STDIN',
      '--loop',
      '--skip-input-header'
        ),
        '0 but true',
        'Insert from CSV OK';

    App::dbc::main( 'sqlite', 'SELECT id, string FROM mytable',
      '--outformat=table' );

    is $stdout_var,
        Mojo::File->new('t/testdata/out/sql-insert-stdin-2.txt')->slurp,
        'SQL from STDIN OK';
  }

  {    # on_connect_execute insert does its thing
    my $stdout_var = '';
    open( my $stdout, '>', \$stdout_var ) or die $!;
    local *STDOUT = $stdout;

    is App::dbc::main('on_connect_magic'), '0 but true', 'Insert from CSV OK';

    is $stdout_var,
        Mojo::File->new('t/testdata/out/on_connect_execute-insert.txt')
        ->slurp,
        'on_connect_execute insert output as expected';
  }

  {    # Param from STDIN, commit-interval=1

    _create_table_uniqs('sqlite');

    my $action_var = '';
    open( my $action, '>', \$action_var ) or die $!;

    no warnings;

    # Simulate slow execute
    my $org_execute = \&DBI::st::execute;
    local *DBI::st::execute = sub {
      my $statement = $_[0]->{Statement};
      say {$action} $statement;

      # Simulate first query being slow
      if ( $statement =~ m/^INSERT INTO uniqs/ ) {
        state $i++;
        sleep 1 if $i == 1;
      }

      return $org_execute->(@_);
    };

    # stop the slow execution after first commit
    my $org_commit = \&DBI::db::commit;
    local *DBI::db::commit = sub {
      say {$action} 'COMMIT';
      return $org_commit->(@_);
    };
    use warnings;

    my $stdin   = join "\n", q{string1}, q{string2}, q{string3};
    my @queries = (
      [ 'sqlite',              'INSERT INTO uniqs (string) VALUES(?)',
        '--commit-interval=1', '--optfile=STDIN',
        '--loop'
      ],
      [ 'sqlite',          'SELECT COUNT(*) FROM uniqs',
        '--outformat=csv', '--skip-output-header'
      ],
      [ 'sqlite',          'SELECT id, string FROM uniqs',
        '--outformat=csv', '--skip-output-header'
      ],
      [ 'sqlite', 'DELETE FROM uniqs', '--verbose=0' ]
    );

    my $stdout_var = _run_and_capture( \@queries, $stdin );

    # Commits appear double because of the callback is triggered after commit
    # is called but before internal commit is executed, and we call commit
    # again from within the callback to execute the real commit
    my $expected_action = trim_indent( '
      INSERT INTO uniqs (string) VALUES(?)
      COMMIT
      COMMIT
      INSERT INTO uniqs (string) VALUES(?)
      INSERT INTO uniqs (string) VALUES(?)
      COMMIT
      COMMIT
      SELECT COUNT(*) FROM uniqs
      SELECT id, string FROM uniqs
      DELETE FROM uniqs
    ' );

    is $action_var, $expected_action, 'actions ok';

    is $stdout_var,
        "rows affected: 3\n3\n1,string1\n2,string2\n3,string3\n",
        'param from STDIN OK';
  }

  {    # Param from STDIN, commit-size=3

    _create_table_uniqs('sqlite');

    my $action_var = '';
    open( my $action, '>', \$action_var ) or die $!;

    no warnings;
    my $org_execute = \&DBI::st::execute;
    local *DBI::st::execute = sub {
      my $statement = $_[0]->{Statement};
      say {$action} $statement;
      return $org_execute->(@_);
    };

    # stop the slow execution after first commit
    my $org_commit = \&DBI::db::commit;
    local *DBI::db::commit = sub {
      say {$action} 'COMMIT';
      return $org_commit->(@_);
    };
    use warnings;

    my $stdin   = join "\n", map {"string$_"} 1 .. 8;
    my @queries = (
      [ 'sqlite',          'INSERT INTO uniqs (string) VALUES(?)',
        '--commit-size=3', '--optfile=STDIN',
        '--loop'
      ],
      [ 'sqlite',          'SELECT COUNT(*) FROM uniqs',
        '--outformat=csv', '--skip-output-header'
      ],
      [ 'sqlite',          'SELECT id, string FROM uniqs',
        '--outformat=csv', '--skip-output-header'
      ],
      [ 'sqlite', 'DELETE FROM uniqs', '--verbose=0' ]
    );

    my $stdout_var = _run_and_capture( \@queries, $stdin );

    # Commits appear double because of the callback is triggered after commit
    # is called but before internal commit is executed, and we call commit
    # again from within the callback to execute the real commit
    my $expected_action = trim_indent( '
      INSERT INTO uniqs (string) VALUES(?)
      INSERT INTO uniqs (string) VALUES(?)
      INSERT INTO uniqs (string) VALUES(?)
      COMMIT
      COMMIT
      INSERT INTO uniqs (string) VALUES(?)
      INSERT INTO uniqs (string) VALUES(?)
      INSERT INTO uniqs (string) VALUES(?)
      COMMIT
      COMMIT
      INSERT INTO uniqs (string) VALUES(?)
      INSERT INTO uniqs (string) VALUES(?)
      COMMIT
      COMMIT
      SELECT COUNT(*) FROM uniqs
      SELECT id, string FROM uniqs
      DELETE FROM uniqs
    ' );

    is $action_var, $expected_action, 'actions ok';

    is $stdout_var,
        sprintf( "rows affected: 8\n8\n%s\n",
      join "\n", map {"$_,string$_"} 1 .. 8 ),
        'param from STDIN OK';
  }
}

done_testing;

sub _run_and_capture {
  my ( $queries, $stdin ) = @_;

  my $stdout = '';
  open( my $stdout_fh, '>', \$stdout ) or die $!;
  local *STDOUT = $stdout_fh;

  open( my $stdin_fh, '<', \$stdin ) or die $!;
  local *STDIN = $stdin_fh;

  foreach my $query ( @{$queries} ) {
    App::dbc::main( @{$query} );
  }

  return $stdout;
}

sub _create_table_uniqs {
  my ($alias) = @_;

  my @queries = (
    'DROP TABLE IF EXISTS uniqs',
    'CREATE TABLE uniqs (
         id       INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
         string   varchar(128),
         UNIQUE(string)
      )
    '
  );

  foreach my $query (@queries) {
    is App::dbc::main( $alias, $query, '--verbose=0' ), '0 but true', $query;
  }

  return 1;
}

sub trim_indent {
  my ($string) = @_;
  $string =~ s/\n\h+/\n/g;
  $string =~ s/^\n|\h+$//g;
  return $string;
}

__END__

