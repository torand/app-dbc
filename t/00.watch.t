use 5.012;
use strict;
use warnings;

use Carp;
use Data::Dumper;
use Mojo::File qw(tempfile);
use Test::Most;

require './bin/dbc';

local $SIG{__DIE__} = sub { confess @_ };

my $tempfile = Mojo::File->new(tempfile);
Mojo::File->new('t/testdata/dbc.conf')->copy_to($tempfile);

local $ENV{APP_DBC_CONFIGFILE} = $tempfile;

ok -p *STDOUT, 'STDOUT is a named PIPE';
ok( !App::dbc::stdout_is_terminal( { outfile => 'STDOUT' }, \*STDOUT ),
  'STDOUT is not a terminal' );

{
  my $query = q{SELECT ? AS 'from', ? AS 'upto'};
  my @params =
      qw(--from 2023-01-01 --upto 2023-01-04 --by day --watch 1 --verbose 2);

  my ( $rc, $stdout ) = do {
    my $called = 0;

    # Override methods to trick the app to believe it is connected to
    # terminal and do graceful exit without a ctrl-z SIGINT signal
    no warnings qw(redefine once);
    local *App::dbc::stdout_is_terminal = sub { return 1 };
    local *App::dbc::_graceful_exit     = sub { return $called++ >= 8 };
    use warnings;

    # trick Term::Screen
    local $ENV{TERM} = 'xterm';

    my $stdout = '';
    open( my $stdout_fh, '>', \$stdout ) or die $!;
    local *STDOUT = $stdout_fh;

    my $rc = App::dbc::main( 'mem', $query, @params );

    ( $rc, $stdout );
  };

  is $rc, App::dbc::ZERO_BUT_TRUE(), 'rc OK';

  my @count_executed = $stdout =~ m/(executed)/g;
  is scalar @count_executed, 2, 'executed twice';

  my @count_last_from =
      $stdout =~ m/(from: [^\v]+ 2023-01-03 00:00:00 \|\n)/g;
  is scalar @count_last_from, 2, '2023-01-03 00:00:00 last element twice';

  my @count_last_upto =
      $stdout =~ m/(upto: [^\v]+ 2023-01-04 00:00:00 \|\n)/g;
  is scalar @count_last_upto, 2, '2023-01-04 00:00:00 last element twice';
}

done_testing;

__END__

