package Test::DBC;

use 5.012;
use strict;
use warnings;

use Carp    qw(confess);
use English qw(-no_match_vars);

sub new {
  bless {}, __PACKAGE__;
}

sub log {
  my ( $self, $new_entry ) = @_;
  return $self->{'log'} if scalar @_ == 1;
  push @{ $self->{'log'} }, $$new_entry;
  return $$new_entry;
}

sub capture_stdout {
  my ( $self, $callback ) = @_;

  local $SIG{__DIE__} = sub { confess @_ };

  local $OUTPUT_AUTOFLUSH = 1;
  my $stdout_var = '';
  open( my $stdout, '>', \$stdout_var ) or die $!;
  local *STDOUT = $stdout;

  my $rc = $callback->( \$stdout_var );

  return $self->log( \$stdout_var );
}

sub trim_indent {
  my ( $self, $string ) = @_;
  $string =~ s/\n\h+/\n/g;
  $string =~ s/^\n|\h+$//g;
  return $string;
}

1;

__END__
