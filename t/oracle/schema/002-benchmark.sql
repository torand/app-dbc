CONNECT system/DummyPW123@localhost/XEPDB1;

BEGIN
  EXECUTE IMMEDIATE 'DROP USER benchmark CASCADE';
  EXECUTE IMMEDIATE 'DROP TABLESPACE benchmark INCLUDING CONTENTS';
EXCEPTION
    WHEN OTHERS THEN
      IF SQLCODE = -1918 THEN
        NULL;
      ELSE
         RAISE;
      END IF;
END;
/

CREATE TABLESPACE benchmark
      DATAFILE 'benchmark.dbf'
      SIZE 100m REUSE
      AUTOEXTEND ON;

CREATE USER benchmark
  IDENTIFIED BY DummyPW123
  DEFAULT TABLESPACE benchmark
  QUOTA UNLIMITED ON benchmark;
GRANT CONNECT, RESOURCE, DBA TO benchmark;

ALTER SESSION SET CURRENT_SCHEMA=benchmark;

CREATE TABLE key_value_lookup (
  key varchar2(50) NOT NULL,
  val varchar2(200),
  CONSTRAINT key_value_lookup_pk PRIMARY KEY (key)
);

CREATE TABLE key_value_updated (
  key varchar(50) NOT NULL,
  val varchar2(200),
  ts  timestamp(6) DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT key_value_updated_pk PRIMARY KEY (key)
);

CREATE TABLE auto_incrementing_table (
  id  number       GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  ts  timestamp(6) DEFAULT CURRENT_TIMESTAMP NOT NULL,
  val varchar2(32)
);

