requires 'Carp'            => 0;
requires 'Clone'           => 0;
requires 'DBI'             => 0;
requires 'English'         => 0;
requires 'Getopt::Long'    => 0;
requires 'Module::Load'    => 0;
requires 'Time::Piece'     => '>=1.32';
requires 'Time::Seconds'   => 0;
requires 'Time::HiRes'     => 0;
requires 'Time::Duration'  => 0;
requires 'Term::Screen'    => 0;
requires 'Term::Size::Any' => 0;

on 'build' => sub {
  requires 'ExtUtils::MakeMaker::CPANfile' => 0;
  requires 'version'                       => 0;
};

on 'test' => sub {
  requires 'Data::Dumper',        => 0;
  requires 'Data::MessagePack',   => 0;
  requires 'DBD::SQLite',         => 0;
  requires 'Devel::Cover',        => 0;
  requires 'JSON::PP',            => 0;
  requires 'List::MoreUtils',     => 0;
  requires 'Mojo::File',          => 0;
  requires 'PerlIO::gzip',        => 0;
  requires 'Test::Most',          => 0;
  requires 'Test::Perl::Critic',  => 0;
  requires 'Test::Pod',           => 0;
  requires 'Test::Pod::Coverage', => 0;
  requires 'Text::CSV',           => 0;
  requires 'YAML',                => 0;
};
