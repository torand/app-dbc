# NAME

dbc - Database Client

# SYNOPSIS

    **This is alpha software, use with caution**

        dbc <db alias> <query> <optional parameters> [
        --query-type=auto(default)|select|insert|replace|update
        --loop
        --from=<date|int> --upto=<date|int> --by=second|minute|hour|day|int
        -- reverse
        --watch=<interval in seconds>
        --informat=auto(default)|csv
        --outformat=auto(default)|vertical|table|column|csv|json|yaml|graph
        --abort=10000
        --vertical=5
        --skip-input-header
        --skip-output-header
        --verbose
        --sqlfile=/path/to/file|STDIN
        --optfile=/path/to/file|STDIN
        --outfile=/path/to/file|STDOUT
        --logfile=/path/to/file
        --commit-interval=1
        --commit-size=100
        --prompt-to-exit=<seconds>
        ]

## Examples

### Single value

        dbc mem "SELECT TIME()"
        00:38:09

### Placeholder query, parameters from command line

    All input parameters used in one execution of the query:

        dbc mem "select ? a, ? b, ? c" 1 2 3 --outformat=table
        +---+---+---+
        | a | b | c |
        +---+---+---+
        | 1 | 2 | 3 |
        +---+---+---+

### Loop flag

    Execute the query once for each input parameter

        dbc mem "select ? n" 1 2 3 --outformat=table --loop
        +---+
        | n |
        +---+
        | 1 |
        | 2 |
        | 3 |
        +---+

### STDIN as source for the parameters

    Parameters can be streamed from STDIN, query is executed at once when loop flag is set.
    If CSV format is used output will rendered with no delay making it possible to work with endless streams.

        perl -E '$|=1; my $i=0; while($i<=3) {say $i++;sleep 1;}' | dbc mem "SELECT TIME(), ? as num" --optfile=STDIN --loop --outformat=csv
        TIME(),num
        00:21:19,1
        00:21:20,2
        00:21:21,3

### Split timelines into manageable sections

    Run the query as a set of queries with smaller timespan increments

        dbc mem "SELECT ? AS 'FROM', ? AS 'UPTO'" --from=2020-01-01 --upto=2020-01-04 --by=day --outformat=table
        +---------------------+---------------------+
        | FROM                | UPTO                |
        +---------------------+---------------------+
        | 2020-01-01 00:00:00 | 2020-01-02 00:00:00 |
        | 2020-01-02 00:00:00 | 2020-01-03 00:00:00 |
        | 2020-01-03 00:00:00 | 2020-01-04 00:00:00 |
        +---------------------+---------------------+

### Split integer ranges into manageable sections

    Run the query as a set of smaller queries, in reverse order

        dbc mem "SELECT ? AS 'FROM', ? AS 'UPTO'" --from=10 --upto=20 --by=2 --reverse --outformat=column
        +----+----+----+----+----+----+
         FROM: 18 | 16 | 14 | 12 | 10 |
         UPTO: 20 | 18 | 16 | 14 | 12 |
        +----+----+----+----+----+----+

## Logfile

    The application can log queries along with parameters and result.

    Note this may require significant storage if you work with large datasets.
    The log must be pruned or reset manually.

    First run a query

        bin/dbc logger 'select n, printf("%.6f",power(1.337,n)) p from (SELECT ? n, ? c) p' --from 1 --upto 16 --by 2 --outformat=column --logfile=/dev/shm/dbc.log.sqlite
        +-+----------+----------+----------+----------+-----------+-----------+-----------+-----------+
         n:        1 |        3 |        5 |        7 |         9 |        11 |        13 |        15 |
         p: 1.337000 | 2.389980 | 4.272254 | 7.636948 | 13.651572 | 24.403127 | 43.622273 | 77.977823 |
        +-+----------+----------+----------+----------+-----------+-----------+-----------+-----------+

    Then inspect the last record of the log

        bin/dbc logger @log recent 1 --logfile=/dev/shm/dbc.log.sqlite
        +---------+--------------------------------------------------------------------+
                id: 406 - 409                                                          |
         timestamp: 1731449176 - 1731449209                                            |
           runtime: 0.000112 avg(sec)                                                  |
          checksum: 62e6b152f8cb6b34e48e9f123e14e4dcc8137929e32dffafdc8bf57d31fa6605   |
             query: select n, printf("%.6f",power(1.337,n)) p from (SELECT ? n, ? c) p |
             param: 65 avg(bytes)                                                      |
            output: 104 avg(bytes)                                                     |
        +---------+--------------------------------------------------------------------+

    In his case I have been running this same query over and over with the last
    it outputs the min and max id, min/max timestamp and average runtime,
    average bytes of params and average bytes of output.

    Note the checksum is set by the query, not the parameters or the output

    When you know the id of a specific query you can inspect that record.
    By setting the --outformat you control the format of both the parameters
    and the output

        bin/dbc logger @log 409 --outformat=csv --logfile=/dev/shm/dbc.log.sqlite
        Query:
        select n, printf("%.6f",power(1.337,n)) p from (SELECT ? n, ? c) p;
        Params:
        1,3
        3,5
        5,7
        7,9
        9,11
        11,13
        13,15
        15,16
        Output:
        n,p
        1,1.337000
        3,2.389980
        5,4.272254
        7,7.636948
        9,13.651572
        11,24.403127
        13,43.622273
        15,77.977823

# DESCRIPTION

Envisioned as an alternative to platform specific database command line clients.

A standard interface for any commonly used features like basic select, insert,
update. With additional super powers like being able to output in common
formats like yaml, json, csv with a simple command line parameter. Also loop
through multiple parameters or parameter sets using placeholders in the queries.

A wrapper around Perl's DBI module, the aims is to be implemented as a single
file with minimal dependencies to make it easy to install/copy to remote machines
when standard install procedures are impractical.

Written in functional style as an experiment.

## Features:

  * Low memory usage, even when working with large data sets (assuming driver and format support it)
  * Read queries and input parameters from STDIN, files or the command line
  * Loop through a list of parameters and show output as single result
  * Loop through long timespans and integer ranges in shorter intervals
  * Support multiple machine readable output formats:
    * CSV
    * YAML
    * JSON
    * Msgpack
  * Human readable formats
    * Table for medium data sets
    * Vertical small data sets with many columns
    * Single elements
  * Logging of queries along with parameters and output
    Logged queries can be inspected and the results can be extracted in
    different formats. A query that took hours to execute, but you outputted
    it in the wrong format? The log store the query output as CSV internally 
    and can be instructed to render it in any of the supported formats.
  * --verbose control verbosity
  * --prompt-to-exit avoid application exiting after long running queries

# INSTALLATION

To install this module, run the following commands:

    perl Makefile.PL
    make
    make test
    make install

Alternatively, to install with Module::Build, you can use the following commands:

    perl Build.PL
    ./Build
    ./Build test
    ./Build install

Or use the bin/dbc file directly.

The driver (DBD:*) module(s) must be installed separately, regardless of
installation method

The JSON, YAML, Text::CSV, Msgpack modules must be installed separately if you
want to use these formats

# DEPENDENCIES

Mandatory:

  * DBI
  * DBD::... for the DB you will use it against

Optional depending on usage:

  * YAML
  * JSON
  * Text::CSV
  * Data::MessagePack
  * Time::Duration
  * Time::HiRes
  * Time::Piece

# TODO

  * Logging
  * * Slow query log
  * * Prune log automatically
  * * Improve granularity of logging, maybe the output is not needed/desired to be logged?
  * * Allow extract of parameters from the log to a file
  * * Replay query and params from log.
  * * inspect specific queries
  * * convert unix timestamp to localtime
  * Add diff function
  * Make graph function operate incrementally on infinite streams
  * Explicit control of character encodings, from file, from db, hex/unhex?
  * Support JSON, YAML from STDIN
  * Implement tasks, a stored query along with in/output parameters + alias
  * Make complex jobs persistent and able to continue from last run
  * Persistent mode and/or daemon - do not depend on tmux
  * Tail files
  * Replace Time::Piece with something builtin or package the module
  * Better create config guide
  * Better config storage format
  * Definable error responses, on disconnect, corrupt statement handles etc.
  * Help and man pages
  * Unify documentation such that it can be written only once

# COPYRIGHT AND LICENCE

Copyright (C) 2021-2025, Tore Andersson

[Click to see license](LICENSE)

